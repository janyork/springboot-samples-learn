package com.hyh.web;

import com.hyh.entity.User;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Summerday
 */

@Api(tags = "User控制器") //修饰整个类，描述Controller的作用
@RestController
@RequestMapping("/users")
public class UserController {

    private static final List<User> USERS = new ArrayList<>();

    static {
        USERS.add(new User(1, "hyh", 12));
        USERS.add(new User(2, "summer day", 18));
        USERS.add(new User(3, "天乔巴夏", 20));
    }

    @GetMapping("/{id}")
    @ApiOperation("获取指定user")
    public User getUser(@PathVariable @ApiParam(value = "id", required = true, defaultValue = "3") Integer id) {
        return USERS.get(id - 1);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除指定user")
    @ApiImplicitParam(name = "id", value = "user id", required = true, dataType = "Integer", paramType = "path")
    public String deleteUser(@PathVariable Integer id) {
        USERS.remove(id - 1);
        return "success";
    }

    @PostMapping()
    @ApiOperation("新增用户")
    public String postUser(@RequestBody User user) {
        USERS.add(user);
        return "success";
    }

    @PutMapping("/{id}")
    @ApiOperation("更新用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "要更新的user id", required = true, dataType = "Integer", paramType = "path"),
            @ApiImplicitParam(name = "user", value = "user 实体", required = true, dataType = "User")
    })
    public String putUser(@PathVariable Integer id, @RequestBody User user) {
        user.setId(id);
        USERS.set(id - 1, user);
        return "success";
    }


    @GetMapping()
    @ApiOperation("用户列表")
    public List<User> getUsers() {
        return USERS;
    }

    @ApiIgnore
    @GetMapping("/ignore")
    public String ignoreTest() {
        return "ignore";
    }
}
