package com.hyh.thymeleaf.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Summerday
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
public class User {

    String id;
    String username;
    Integer age;
}
