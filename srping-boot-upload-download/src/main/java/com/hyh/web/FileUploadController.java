package com.hyh.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 文件上传
 *
 * @author Summerday
 */
@Controller
public class FileUploadController {

    private static final String UPLOADED_FOLDER = System.getProperty("user.dir");

    @GetMapping("/")
    public String index() {
        return "file";
    }

    @PostMapping("/upload")
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) throws IOException {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("msg", "文件为空,请选择你的文件上传");
            return "redirect:uploadStatus";

        }
        saveFile(file);
        redirectAttributes.addFlashAttribute("msg", "上传文件" + file.getOriginalFilename() + "成功");
        redirectAttributes.addFlashAttribute("url", "/upload/" + file.getOriginalFilename());
        return "redirect:uploadStatus";
    }

    private void saveFile(MultipartFile file) throws IOException {
        Path path = Paths.get(UPLOADED_FOLDER + "/" + file.getOriginalFilename());
        file.transferTo(path);
    }

    @GetMapping("/uploadStatus")
    public String uploadStatus() {
        return "uploadStatus";
    }
}
