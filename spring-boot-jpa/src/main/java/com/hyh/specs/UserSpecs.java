package com.hyh.specs;

import com.hyh.entity.User;
import org.springframework.data.jpa.domain.Specification;

/**
 * @author Summerday
 */
public class UserSpecs {

    public static Specification<User> contains(String attribute, String value) {
        return (root, query, builder) -> builder.like(root.get(attribute), "%" + value + "%");
    }


}
