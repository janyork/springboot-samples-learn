package com.hyh.validator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.SocketTimeoutException;
import java.util.Properties;

@SpringBootApplication
public class SpringBootValidatorFormApplication {

    public static void main(String[] args) {

        SpringApplication.run(SpringBootValidatorFormApplication.class, args);
    }

}
