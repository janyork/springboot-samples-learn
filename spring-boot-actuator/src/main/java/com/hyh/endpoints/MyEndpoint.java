package com.hyh.endpoints;

import org.springframework.boot.actuate.endpoint.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Summerday
 */
@Component
@Endpoint(id = "myEndPoint")
public class MyEndpoint {

    Runtime runtime = Runtime.getRuntime();

    @ReadOperation
    public Map<String, Object> getData() {
        Map<String, Object> map = new HashMap<>();
        map.put("available-processors", runtime.availableProcessors());
        map.put("free-memory", runtime.freeMemory());
        map.put("max-memory", runtime.maxMemory());
        map.put("total-memory", runtime.totalMemory());
        return map;
    }

    @WriteOperation
    public Map<String,Object> updateData(String name){
        Map<String,Object> map = new HashMap<>();
        map.put("name",name.toUpperCase());
        return map;
    }

    @DeleteOperation
    public void deleteData(){

    }
}
