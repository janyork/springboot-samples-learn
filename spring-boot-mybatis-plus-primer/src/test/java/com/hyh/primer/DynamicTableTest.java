package com.hyh.primer;

import com.hyh.primer.entity.User;
import com.hyh.primer.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
class DynamicTableTest {

    @Resource
    UserMapper userMapper;

    @Test
    void select(){
        // 配置了之后,会随机访问user_2019和user_2018
        for (int i = 0; i < 6; i++) {
            User user = userMapper.selectById(1);
            System.out.println(user.getName());
        }
    }

}
