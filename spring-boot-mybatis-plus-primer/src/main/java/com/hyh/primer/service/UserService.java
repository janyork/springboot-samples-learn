package com.hyh.primer.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.hyh.primer.entity.User;

import java.util.List;

/**
 * Service接口,继承IService
 * @author Summerday
 */
public interface UserService extends IService<User> {

    List<User> mySelectList();
}
