package com.hyh.primer.config;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.extension.injector.methods.AlwaysUpdateSomeColumnById;
import com.baomidou.mybatisplus.extension.injector.methods.InsertBatchSomeColumn;
import com.baomidou.mybatisplus.extension.injector.methods.LogicDeleteByIdWithFill;
import com.hyh.primer.methods.DeleteAll;
import com.hyh.primer.methods.FindOne;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 自定义sql注入
 * @author Summerday
 */
@Component
public class MySqlInjector extends DefaultSqlInjector {

    @Override
    public List<AbstractMethod> getMethodList(Class<?> mapperClass) {

        // 默认已经存在方法列表
        List<AbstractMethod> methodList = super.getMethodList(mapperClass);
        //增加自定义方法
        methodList.add(new DeleteAll());
        methodList.add(new FindOne());

        /*
         * 以下 3 个为内置选装件
         * 头 2 个支持字段筛选函数
         */
        // 例: 不要指定了 update 填充的字段  排除逻辑删除,和"age"字段
        methodList.add(new InsertBatchSomeColumn(i -> i.getFieldFill() != FieldFill.UPDATE && !i.isLogicDelete() && !"age".equals(i.getColumn())));
        // 筛选字段
        methodList.add(new AlwaysUpdateSomeColumnById(i -> !"age".equals(i.getColumn())));
        methodList.add(new LogicDeleteByIdWithFill());
        return methodList;
    }
}
