package com.hyh.web;

import com.hyh.config.AcmeProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;

/**
 * @author Summerday
 */

@Component
public class HelloComponent {


    private final AcmeProperties acmeProperties;

    @Autowired
    public HelloComponent(AcmeProperties properties) {
        this.acmeProperties = properties;
    }

    @PostConstruct
    public void openConnection() {
        System.out.println(acmeProperties);
    }


}
