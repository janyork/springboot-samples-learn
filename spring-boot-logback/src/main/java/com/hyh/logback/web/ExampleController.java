package com.hyh.logback.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Summerday
 */

@RestController
public class ExampleController {

    @GetMapping("/ex")
    public String ex(){
        return "ex";
    }
}
