package com.hyh.exhand.controller;

import com.hyh.exhand.entity.AjaxResult;
import com.hyh.exhand.exception.CustomException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Summerday
 */
@RestController
public class TestController {

    /**
     * 模拟用户数据访问
     */
    @GetMapping("/")
    public String index() throws Exception {
        int a = 1 / 0;
        return "success";
    }

    @GetMapping("/ajax")
    public AjaxResult ajax() {
        double alpha = 0.9;
        if (Math.random() < alpha) {
            throw new CustomException("自定义异常!");
        }
        return AjaxResult.ok();
    }
}
