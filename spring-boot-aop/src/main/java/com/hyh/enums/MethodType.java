package com.hyh.enums;

/**
 * 操作类型
 * @author Summerday
 */
public enum MethodType {

    /**
     * 其它
     */
    OTHER,

    /**
     * 新增
     */
    INSERT,

    /**
     * 修改
     */
    UPDATE,

    /**
     * 删除
     */
    DELETE

}
