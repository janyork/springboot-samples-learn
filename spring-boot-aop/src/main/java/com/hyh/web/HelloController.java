package com.hyh.web;

import com.hyh.annotation.Log;
import com.hyh.entity.User;
import com.hyh.enums.MethodType;
import org.springframework.web.bind.annotation.*;

/**
 * @author Summerday
 */

@RestController
public class HelloController {

    @PostMapping("/hello")
    @Log(description = "hello post",methodType = MethodType.INSERT)
    public String hello(@RequestBody User user) {
        return "hello";
    }

    @GetMapping("/hello")
    @Log(description = "hello get")
    public String hello(@RequestParam("name") String username, String hobby) {
        int a = 1 / 0;
        return "hello";
    }
}
