package com.hyh.h2.entity;

import lombok.Data;


/**
 * @author Summerday
 */
@Data
public class User {
    private Long id;
    private String name;
    private Integer age;
    private String email;
}
